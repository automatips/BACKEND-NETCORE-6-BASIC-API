﻿using System.ComponentModel.DataAnnotations;

namespace PremoAPI
{
    public class Status
    {

        [StringLength(20)]
        public string Description { get; set; } = string.Empty;

        public StatusType? StatusType { get; set; } 
    }

    public class StatusType
    {
        public string Description { get; set; } = string.Empty;
    }
}
