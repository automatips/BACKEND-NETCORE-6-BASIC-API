﻿using System.ComponentModel.DataAnnotations;

namespace PremoAPI
{
    public class Project
    {
        [StringLength(20)]
        public int Id { get; set; }

        [StringLength(200)]
        public string? Description { get; set; }

        [StringLength(200)]
        public string? Location { get; set; }

        public Country? Country { get; set; }

        public Client? Client { get; set; }

        public User? User { get; set; }

        public DateTime StartBuildDate { get; set; }

        public DateTime FinishBuildDate { get; set; }

        [StringLength(200)]
        public string? Scope { get; set; }

        public ProjectSPO? ProjectSPO { get; set; }

        public Status? Status { get; set; }

        public Segment? Segment { get; set; }

        [StringLength(20)]
        public int ComparativeAmount { get; set; }

        [StringLength(20)]
        public int ExWorksCost { get; set; }
        
        [StringLength(20)]
        public int FOBCost { get; set; }

        [StringLength(20)]
        public int FleteInt { get; set; }

        [StringLength(20)]
        public int InsuranceInt { get; set; }

        [StringLength(20)]
        public int AACost { get; set; }

        [StringLength(20)]
        public int FixedValue { get; set; }
    }

    public class Country
    {
    }
    public class User
    {
    }
    public class ProjectSPO
    {
    }
    

}
