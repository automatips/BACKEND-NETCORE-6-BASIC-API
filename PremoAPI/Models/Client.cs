﻿using System.ComponentModel.DataAnnotations;

namespace PremoAPI
{
    public class Client
    {
        public int Id { get; set; }

        [StringLength(20)]
        public string RazonSocial { get; set; } = string.Empty;

        [StringLength(200)]
        public Identification? Identification { get; set; }
    }

    public class Identification
    {
        [StringLength(200)]
        public int Description { get; set; }

    }

    public class Segment
    {
        [StringLength(200)]
        public string? Description { get; set; }
    }
}
