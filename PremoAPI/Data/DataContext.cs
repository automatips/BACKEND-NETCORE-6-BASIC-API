﻿using Microsoft.EntityFrameworkCore;

namespace PremoAPI.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Client> Clients { get; set; }
    }
}
